class Singleton(type):
    """
    This metaclass only ensures that child classes will be Singletons
    """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Config(metaclass=Singleton):
    """
    Singleton that holds all configuration for the tool
    """
    def __init__(self,
            app: str = None,
            function: str = 'main',
            model: str = None,
            isa: str = 'RV64IMAFDC',
            reports_path: str = './reports',
        ):
        """
        Configuration:

        :param app: None;None;file :Inform the RISC-V binary application
        :param isa: -i;--isa;str :Define the RISC-V ISA of the binary [Default RV64IMAFDC]
        :param function: -f;--function;str :Choose the function that will be the profiling start point [Defult main]
        :param model: -m;--model;file :Set the json model for perfomance evaluation [Example models/exemple.json]
        :param reports: -r;--reports;folder :Set the reports folder [Defult ./reports]
        :param log: -l;--log;file :Insert an app log generated previously
        :param dump: -d;--dump;file :Insert an app dump generated previously
        :param template: -t;--template;bool :Create a core template model from app [Default False]
        :param nopk: -np;--nopk;bool :Set the profile without proxy kernel for bare-metal apps [Default False]
        :param verbose: -v;--verbose;bool :Show profile details from execution stack [Default False]

        """
        self.app          = app
        self.reports_path = reports_path
        self.binary_name  = None
        self.binary_path  = None
        self.model_name   = None
        self.model_path   = None
        self.model        = model
        self.function     = function
        self.isa          = isa
        self.arch         = '64'
        self.log_path     = None
        self.dump_path    = None
        self.template     = False
        self.verbose      = False
        self.nopk         = False
