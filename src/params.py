import re

from src.config import Config


class Params():
    """
    Class for configuration params processing
    """
    def __init__(self,
                doc: str = None,
            ):
        self.params_list  = []
        params_doc = Config.__init__.__doc__.split(':')[2:]
        params_quant = (int)(len(params_doc) / 3)

        for i in range(params_quant):
            self.params_list.append([])
            app_index = i*3
            self.params_list[i].append(params_doc[app_index].split(' ')[-1])
            self.params_list[i].append(params_doc[app_index + 1].strip())
            self.params_list[i].append(params_doc[app_index + 2].split('\n')[0])  
        
        #print(self.params_list)

class ParamsItem:
    """
    Class for item params processing
    """
    def __init__(self,
                item_params: list = [],
            ):
       
        options = item_params[1].split(';')
        self.item = item_params[0]
        self.alias = options[0]
        self.arg = options[1]
        self.type = options[2]
        self.description = item_params[2]
