import json
from pathlib import Path
from datetime import datetime
import logging
from typing import List

from src.config import Config

class ReportArtifact:
    def __init__(self, data_dict, label):
        self._data_dict = data_dict
        self._label = label

    def generate_report(self, out_file: str):
        pass

class JsonArtifact(ReportArtifact):

    def generate_report(self, path: str):
        out_filename = path + '/' + self._label + '.json'
        with open(out_filename, 'w') as out_file:
            json.dump(self._data_dict, out_file, indent=2)
        

class Reporter:
    """
    Class that handles report generation
    """

    def __init__(self):
        self._artifacts: List[ReportArtifact] = []
        self._log = logging.getLogger('reporter')

    def register_data(self, data_dict: dict, label: str, type: str = 'json'):
        """
        Registers a report data. 
        
        :param data_dict:   data for the artifact - see supported types below
                            for details.
        :param label:       label for the artifact

        Supported types are:
        - JSON files: data_dict is a python dictionary that will be written
                      as a JSON file.
        """
        if type == 'json':
            self._artifacts.append(JsonArtifact(data_dict, label))
        else:
            self._log.error('invalid data type in reporter: ' + str(type))

    def generate_report(self):
        """
        Generate output files for all registered data artifacts on their
        respective files.

        This will create a directory at <reports_path>/<binary_name>/<time>
        which contains all reports for this run.
        """
        config = Config()
        bin_name = config.binary_name.split('.')[0]
        reports_path = config.reports_path
        dir_name = reports_path + '/' + \
                bin_name + '/' + datetime.now().strftime("%Y-%m-%d-%Hh%M")

        self._log.info('generating report directory: ' + dir_name)
        # Create sub directory for this run
        Path(dir_name).mkdir(parents=True, exist_ok=True)
        # Generate all reports in the created directory
        for artifact in self._artifacts:
            artifact.generate_report(dir_name)
