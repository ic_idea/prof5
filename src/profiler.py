import json
import re
import sys
import os
import copy
from typing import Union
import logging

from src.config import Config
from src.reporter import Reporter

class Profiler:

    def __init__(self, reporter: Reporter):
        self._log = logging.getLogger('profiler')
        self.config = Config()
        self.app = self.config.app
        self.reporter = reporter
        self.function = self.config.function
        self.model = self.config.model
        self.log_name = "prof5_" + self.config.binary_name + ".log"
        self.pkstr = "$(which pk)"
        if self.config.nopk:
            self.pkstr = ""

        self.cmd_log = "spike --isa=%s -l %s %s" % (self.config.isa, 
                                                    self.pkstr, 
                                                    self.config.app)

        self.cmd_log += " 1> /dev/null 2> prof5.log"
 
        self.cmd_dump = "riscv%s-unknown-elf-objdump -d %s" % (self.config.arch, 
                                                            self.config.app)
        self.cmd_dump += " > prof5.dump && echo >> prof5.dump" 
        
        if self.config.verbose:
            print(self.cmd_log)
            print(self.cmd_dump)

    def run(self):
        if not self.config.log_path:
            print("[1] Generating Log")
            self._log.info('Generating Log')
            self._log.debug('Command: ' + str(self.cmd_log))
            returned_value = os.system(self.cmd_log)
            self.config.log_path = 'prof5.log'
        else:
             print("[1] Log from", self.config.log_path)           
        print("[2] Log finished!")
        
        if not self.config.dump_path:
            print("[3] Generating dump...")
            self._log.info('Generating dump')
            self._log.debug('Command: ' + str(self.cmd_dump))
            returned_value = os.system(self.cmd_dump) 
            self.config.dump_path = 'prof5.dump'
        else:
            print("[3] Dump from", self.config.dump_path)            
        print("[4] Dump finished!")
        
        print("[5] Separating functions")
        self._log.info('Separating functions')
        funcs_graph_json = self.tracking_funcs(self.config.dump_path)
        self.reporter.register_data(funcs_graph_json, 'prof5_funcs')
        
        print("[6] Profiling...")   
        prof_dic = self.profiling(funcs_graph_json, self.function, self.config.log_path) 
        model_template = self.gen_model_template(prof_dic)

        if self.config.template:
            self.reporter.register_data(model_template, 'model_template')

        self.reporter.register_data(prof_dic, 'prof5_count')

        if self.config.model:
            print("[7] Evaluating...")
            self._log.info('Evaluating power model')
            results = self.evaluating(self.config.model, prof_dic)
            self.reporter.register_data(results, 'prof5_results')

        self.reporter.generate_report()

        print("Prof5: Finished! - Outupts were redirected to:", self.config.reports_path)   
    

    def tracking_funcs(self, dump_path='prof5.dump'):
        file1 = open(dump_path,  'r')
        func_id = 0
        func_info = []
        graph = {}

        while True:
            line = file1.readline()
            s_func = re.search('<*>:', line)

            if s_func is not None:
                func_info  = line.replace('<','').replace('>','').replace(':','').replace('\n','').split(' ')
                parent = func_info[1]
                parent_init_addr = func_info[0]
                if not (parent in graph):
                    graph[parent] = {}
                    graph[parent]["parents"] = []
                    graph[parent]["childs"] = []

                graph[parent]["init_addr"] = int(parent_init_addr, 16)
     
                while True:
                    end_line = line
                    line = file1.readline()
                    word_count = len(line.split())
                    if line == '\n' or word_count < 2:
                        end_addr = end_line.replace(' ', '').split(':')[0] # substituir por strip()
                        graph[parent]["end_addr"] = int(end_addr, 16)
                        break

                    s_child = re.search('<*>', line)
                    check_branch = re.search("[+]", line) # avoiding branchs
                    ret = re.search('ret', line)
                    if s_child is not None and check_branch is None:
                        child = line.replace('<','').replace('>','').split(' ')[-1] 
                        child = child.replace('\n','')
                        graph[parent]["childs"].append(child)
                        if child in graph:
                            graph[child]["parents"].append(parent)
                        else:
                            graph[child] = {}
                            graph[child]["parents"] = [parent]
                            graph[child]["childs"] = []

                    if ret is not None:
                        ret_addr = line.replace(' ', '').split(':')[0]
                        ret_addr = int(ret_addr, 16)

                        if "ret_addrs" in graph[parent]:
                            graph[parent]["ret_addrs"].append(ret_addr)
                        else:
                            graph[parent]["ret_addrs"] = [ret_addr]

            if not line:
                break
             
        file1.close()
        return graph


    def check_addrs(self, line, addrs):
        for i in addrs:
            if re.search(i, line):
                return True
        return None

    def check_addrs_16(self, line, addrs):
        addr = line.split(' ')[4]
        if not '0x' in addr:
            return False

        addr = int(addr, base=16)

        for i in addrs:
            if int(i, base=16) == addr:
                return True
        return None


    def global_counting(self, funcs_graph_json, prof_dic, base_func, parent):
        parent_cp = copy.deepcopy(parent)
        if not ("global_counter" in prof_dic[parent_cp]):
            prof_dic[parent_cp]["global_counter"] = 1
        
        # Stop condition is when we found the base function (commonly: main)
        while ("parent" in funcs_graph_json[parent_cp]):
            prof_dic[parent_cp]["global_counter"] += 1
       
            # It is a recursive function
            for i in reversed(funcs_graph_json[parent_cp]["parent"]):
                if i != parent_cp:
                    parent_cp = i
                    break
        
        # counting global function
        prof_dic[parent_cp]["global_counter"] += 1

    def in_range(self, funcs_graph_json, f, addr):
        init_addr = funcs_graph_json[f]["init_addr"]
        end_addr = funcs_graph_json[f]["end_addr"]
        
        # Treating exception epc instructions
        if type(addr) == str:
            return True

        if(addr >= init_addr and addr <= end_addr):
            return True
        return False


    def counting(self, prof_dic, exec_stack, parent, insn):
        prof_dic[parent]["self_counter"] += 1

        for f in exec_stack:
            prof_dic[f]["global_counter"] += 1

        if insn in prof_dic[parent]:
            prof_dic[parent][insn] += 1
        else:
            prof_dic[parent][insn] = 1

    def profiling(self, funcs_graph_json, base_func, log_path='prof5.log'):
        if not (base_func in funcs_graph_json):
            return { "message": "This function does not exits in the function dictionary" }

        file1 = open(log_path, 'r')
        prof_dic = {}
        line = ''
        base_func_init_addr = funcs_graph_json[base_func]["init_addr"]
        base_func_ret_addrs  = funcs_graph_json[base_func]["ret_addrs"]
        parent = parent_init_addr = parent_ret_addrs = None
        child = child_init_addr = child_ret_addrs = None

        # Implement a function that is out of primary function
        prof_dic["trap_handling"] = {}
        prof_dic["trap_handling"]["self_counter"] = 0
        prof_dic["trap_handling"]["global_counter"] = 0


        # Finding the initial computation of the function
        while True:
            line = file1.readline()
            addr = line.split(' ')[4]
            if '0x' in addr:
                addr = int(addr, 16)
            s_base_init_addr = base_func_init_addr == addr
            if s_base_init_addr:
                break
            
            if not line:
                return { "message": "The function was not called" }
        
        # Initializing the base function
        insn = line.split(" ")[6]
        prof_dic[base_func] = {}
        prof_dic[base_func]["self_counter"] = 1
        prof_dic[base_func]["global_counter"] = 1
        prof_dic[base_func]["calls"] = 1
        prof_dic[base_func][insn] = 1
        
        # Setting the base functio as the current function 
        parent = base_func
        parent_init_addr = base_func_init_addr
        parent_ret_addrs = base_func_ret_addrs
        exec_stack = [base_func]
 
        while True:
            line = file1.readline()

            # Break if the entire log was analyzed 
            if not line:
                #print("Entire log analyzed")
                break

            line_splited = line.split(" ")
            insn = line_splited[6].replace('\n','')
            current_addr = line_splited[4]
          
            if '0x' in current_addr:
                current_addr = int(current_addr, 16)
           
            # Discarding lines out of the pattern (generally tval attributions)
            if insn == "":
                prof_dic["trap_handling"]["global_counter"] += 1
                continue

            parent = exec_stack[-1]
            
            if self.config.verbose:
                print(' --> '.join(exec_stack))

            # Verifying if the current address is a return
            if current_addr in base_func_ret_addrs:
                #print("Base return address found")
                self.counting(prof_dic, exec_stack, parent, insn)

                # Treating Recursive calls 
                line = file1.readline()
                line_splited = line.split(" ")
                insn = line_splited[6].replace('\n','')
                current_addr = line_splited[4]
                if '0x' in current_addr:
                    current_addr = int(current_addr, 16)

                if self.in_range(funcs_graph_json, parent, current_addr):
                    prof_dic[parent]["calls"] += 1
                    self.counting(prof_dic, exec_stack, parent, insn)
                    continue
                
                break
            
            if "ret_addrs" in funcs_graph_json[parent]:
                if current_addr in funcs_graph_json[parent]["ret_addrs"]:
                    #counting
                    self.counting(prof_dic, exec_stack, parent, insn)
                    exec_stack.pop()
                    parent = exec_stack[-1]
                    continue

            # break if return base is found
            # saltando para uma funcao sem dar retorno
            if not self.in_range(funcs_graph_json, parent, current_addr):
                child_found = False
                for i in funcs_graph_json[parent]["childs"]:
                    if i in funcs_graph_json:
                        if not "init_addr" in funcs_graph_json[i]:
                            continue
                        
                        # Verificar se o filho tem endereco de inicio
                        if self.in_range(funcs_graph_json, i, current_addr):
                        #if current_addr == funcs_graph_json[i]["init_addr"]:
                            child_found = True
                            if not (i in prof_dic):
                                prof_dic[i] = {}
                                prof_dic[i]["global_counter"] = 0
                                prof_dic[i]["self_counter"] = 0
                                prof_dic[i]["calls"] = 0

                            prof_dic[i]["calls"] += 1
                            parent = i
                            exec_stack.append(i)
                            if self.config.verbose:
                                print(' --> '.join(exec_stack))
                            break
                
                if not child_found:
                # Tratar se parent for base func nao tiver no range e nao e uma filha dela
                    if parent == base_func: # pode existir um problema se tiver uma excecao na main
                        #print("Out of basefunc tree", line, exec_stack)
                        a = 1
                        #break
                    else: # posso verificar se e realmente o pai da funcao atual
                        # por enquanto estou assumindo que e um pai
                        #if self.in_range(funcs_graph_json, parent, current_addr)
                        imparent = False
                        inthestack = False
                        inthegraph = True

                        for p in funcs_graph_json[parent]["parents"]:

                            if self.in_range(funcs_graph_json, p, current_addr):
                                imparent = True
                                exec_stack.pop()
                                break
                        #print(exec_stack)
                        if not imparent:
                            for i, f in enumerate(exec_stack):
                                if self.in_range(funcs_graph_json, f, current_addr):
                                    exec_stack = exec_stack[:i]
                                    inthestack = True
                                    break
                        
                        if not inthestack:
                            inthegraph = False
                            for f in funcs_graph_json:
                                if not "init_addr" in funcs_graph_json[f]:
                                    continue
                                if not "end_addr" in funcs_graph_json[f]:
                                    continue
                                if self.in_range(funcs_graph_json, f, current_addr):
                                    inthegraph = True
                                    exec_stack.pop()
                                    exec_stack.append(f)
                                    if self.config.verbose:
                                        print(' --> '.join(exec_stack))
                                    if not (f in prof_dic):
                                        prof_dic[f] = {}
                                        prof_dic[f]["global_counter"] = 0
                                        prof_dic[f]["self_counter"] = 0
                                        prof_dic[f]["calls"] = 1
                                   
                                    break
                        # if not inthegraph:
                        #     print(line)

            parent = exec_stack[-1]
            self.counting(prof_dic, exec_stack, parent, insn)                       

        file1.close()
        return prof_dic   

    def evaluating(self, model, prof_dic):
        with open(model) as json_file:
            model_json = json.load(json_file)
            core_name = model_json["core"]
            freq = model_json["freq"] * 10e6 # MHz 
            power = model_json["power"] * 0.001 # miliwatt
            insns = []
            
            for i in model_json["insns"]:
                insns.append(i)

            results = {}
            not_registered = []
            for f in prof_dic:
                results[f] = { "cycles": 0, "power": 0 }
                for i in prof_dic[f]:
                    if i in insns:
                        i_cycle = model_json["insns"][i]["cycles"]
                        i_power = model_json["insns"][i]["power"]
                        results[f]["cycles"] += i_cycle * prof_dic[f][i]
                        results[f]["power"]  += i_power * prof_dic[f][i] * i_cycle
                    else:
                        not_registered.append(i)

            #print(not_registered)
            c_all = 0       
            l_all = 0
            e_all = 0
            p_all = 0

            for f in results:
                results[f]["latency"] = results[f]["cycles"] * (1 / freq)
                results[f]["energy"]  = results[f]["latency"] * results[f]["power"]
                c_all += results[f]["cycles"]
                l_all += results[f]["latency"]
                e_all += results[f]["energy"]
                p_all += results[f]["power"]

            results["all"] = {}
            results["all"]["IPC"] = prof_dic[self.config.function]["global_counter"] / c_all
            results["all"]["func_counter"] = len(prof_dic)
            results["all"]["cycles"]  = c_all
            results["all"]["latency"] = l_all
            results["all"]["energy"]  = e_all 
            results["all"]["power"]  = p_all
            results["all"]["avg_power_cycle"]  = p_all / c_all


            return results

    def gen_model_template(self, prof_dic):
        model_json = {}
        model_json["core"] = "core-template"
        model_json["freq"] = 1000
        model_json["power"] = 0
        model_json["insns"] = {}

        for f in prof_dic:
            for i in prof_dic[f]:
                if re.search("counter", i) or re.search("calls", i):
                    continue
                if not (i in model_json["insns"]):
                    model_json["insns"][i] = { "cycles": 1,"power": 0 }

        return model_json

    def print_insns(self, prof_dic):
        i_json = {}
        insns = []

        for f in prof_dic:
            for i in prof_dic[f]:
                if not (i in insns):
                    i_json[i] = 0
                else:
                    insns.append(i)

        #print(json.dumps(i_json))
        return i_json
