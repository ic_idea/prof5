import numpy as np
import matplotlib.pyplot as plt
import json
  
# Opening JSON file
with open('prof5_count.json') as json_file:
    data = json.load(json_file)
    # Print the type of data variable
    #print("Type:", type(data))
    #print(data)
    #del data["fftwf_the_planner"]
    functions = [i for i in data]
    counters = [data[i]["global_counter"] for i in data]
    plt.figure(figsize=(15, 9))
    plt.xticks(rotation=90)
    plt.bar(functions, counters, color='royalblue', edgecolor="black")
    plt.show()

    functions = [i for i in data]
    counters = [data[i]["self_counter"] for i in data]
    plt.figure(figsize=(15, 9))
    plt.xticks(rotation=90)
    plt.bar(functions, counters, color='grey', edgecolor="black")
    plt.show()

    function = "execute"
    insns = []
    counters = []
    for i in data[function]:
        if i != "global_counter" and i != "self_counter":
            insns.append(i)
            counters.append(data[function][i])

    #insns = [i for i in data[function]]
    #counters = [data[function][i] for i in data[function]]
    plt.figure(figsize=(15, 9))
    plt.bar(insns, counters, color='seagreen', edgecolor="black")
    plt.xticks(rotation=90)
    plt.show() 
