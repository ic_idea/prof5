Prof5: A RISC-V profiler
==========================

About
-------------

Prof5 is a RISC-V profiler that tracks functions and evaluates each one based on a pre-defined model. 
This is possible because it uses both Spike (RISC-V ISA Simulator) and the RISC-V toolchain to break down the application execution.

Pre-requisites
--------------

Prof5 is entirely built in Python but, as mentioned befeore, it calls the Spike to extract the log execution and riscv `obj-dump` for function register.
So, for Prof5 execution is necessary to install:

- **Python3**: <https://www.python.org/downloads>
- **RISC-V toolchain**: <https://github.com/riscv-collab/riscv-gnu-toolchain>
- **Spike (RISC-V ISA Simulator)**: <https://github.com/riscv-software-src/riscv-isa-sim>
- **Proxy Kernel (RISC-V PK)**: <https://github.com/riscv-software-src/riscv-pk>


Usage
--------
Prof5 assumes you have a RISC-V application compiled. Performing that command:
   
`$ ./prof5 <app>`

Prof5 will generate three JSON (Javascript Object Notation) files in `./reports/<app>/<date>` (Default):

* **model_template.json**: Template for perfomance evaluation
* **prof5_count.json**: Functions and their instruction counters  
* **prof5_funcs.json**: Function tree including initial and return addresses

Options
--------
Among the options available to customize are:

1. Define the RISC-V ISA of the binary  [Default RV64IMAFDC]: `./prof5 <app> -i <isa>`

2. Choose the function that will be the profiling start point [Defult main]: `./prof5 <app> -f <function>`

3. Set the reports folder [Defult ./reports]: `./prof5 <app> -r <report-folder>`

4. Insert an app log generated previously: `./prof5 <app> -l <spike-log>`

5. Insert an app dump generated previously: `./prof5 <app> -d <obj-dump>

6. Create a core template model from app [Default False]: `./prof5 <app> -t

7. Set the profile without proxy kernel for bare-metal apps [Default False]: `./prof5 <app> -np`

8. Show profile details from execution stack [Default False]: `./prof5 <app> -v`

Model
--------

With the template generated (`model_template.json`) you can fill the default fields with the own Core caracheterisctics:

```
{
  "core": <core-name>,
  "freq": <frequency-in-Mhz>,
  "power": <power-in-mW>,
  "insns": {
    "insn": {
      "cycles": <cycles>,
      "power": <power-in-mW>
    },  ... 
  }
}
```

And utilize as a Prof5 parameter: `$ ./prof5 <app> -m <model>.json`

This command generates **prof5_results.json** contained performance stats of your app, based on the model injected.

	
Compiling and Profiling a Simple C Program
-------------------------------------------

Write a short C program and name it hello.c. Then, compile it into a RISC-V ELF binary named hello:
   
`$ riscv64-unknown-elf-gcc -o hello hello.c`
   
Now you can profile the program:

`$ ./prof5 hello`

Execution prof5 outputs will be available on `./reports/hello/<profile-hour/<date>` or on a custom folder already setted (using flag -r).

Docker container with Prof5
-------------------------------------------
A docker container with Prof5 is available on: <https://hub.docker.com/r/jevsilv/prof5>.
In this container is possible to call prof5 everywhere (It is in the PATH).

To pull docker you need to run: 

`$ sudo docker pull jevsilv/prof5:1.0`

To create the instance: 

`$ sudo docker run --name <container-name> -it jevsilv/prof5:1.0 bash`

#### Some tips:
* Start the container created: `sudo docker start <container-name>`
* Attach the container started: `sudo docker attach <container-name>`
* Exit of the container without power off it: CTRL + P