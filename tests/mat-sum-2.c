#include<stdio.h>
#define SIZE 5000

void mult(int * A) { 
    int i;
    for (i = 0; i < SIZE; i++) {
        A[i] = i*10;
    }   
}

void acc(int * A, volatile int * sum) { 
    int i;
    for (i = 0; i < SIZE; i++) {
        sum += A[i];
    }
}

int main() {
    volatile int sum = 0;
    int A[SIZE], B[SIZE];

    mult(A);
    acc(A, &sum);

    mult(B);
    acc(B, &sum);

    return 0;
}
