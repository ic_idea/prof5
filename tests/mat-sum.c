#include<stdio.h>
#define SIZE 5000

int main() {
    volatile int sum = 0;
    int i;
    int A[SIZE];

    for (i = 0; i < SIZE; i++) {
        A[i] = i*10;
    }   

    for (i = 0; i < SIZE; i++) {
        sum += A[i];
    }

    return 0;
}
